## Your Mission

Your mission is to create an app in Go that compiles a single Java source file into Java class file(s). The app should be provided with the path to the source .java file as a command line arg, and output the generated .class file(s).

### Bonus Mission no 1

If time allows, the code should be modified to output alongside the Java class file(s) a .json file that contains the following:

```json
{
  "errors": {
    "compile": ["array of errors from javac if any"]
  },
  "stats": {
    "loc": 20,
    "time": 120
  }
}
```

Where `errors.compile` is an array of errors, and the stats object contains the number of lines of code, `loc`, in the source file and the time, `time`, in milliseconds it took to compile the source.

### Bonus Mission no 2

If time allows, the code should be modified to take the output `.class` file(s) and package them into a `jar` (no class files should be left on the file system).

## Testing

A number of .java files are in the `tests` directory, these may compile, or may not!

## Top Tips

We are looking for sound approach, clarity and quality over getting everything done at speed. Commits are an important measure of such things. You're also expected to take initiative on things not specified, or left out of the spec, what have we missed that we should have thought of?
