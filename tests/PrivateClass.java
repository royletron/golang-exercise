private class PrivateClass {
  public double sum(double... args) {
    double sum = 0.00;
    for (double arg : args) {
      sum += arg;
    }
    return sum;
  }

  public int sum(int... args) {
    int sum = 0;
    for (int arg : args) {
      sum += arg;
    }
    return sum;
  }
}
