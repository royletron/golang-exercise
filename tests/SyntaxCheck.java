public class SyntaxCheck {
  private final String s = "StringTest";
  private int ii = 0;
  private Integer ii2 = 42432;
  private float ff = 0.00f;
  private Float ff2 = 123.456f;
  private double dd = 40.76458;
  private Double dd2 = null;
  private boolean bb = true;
  private Boolean bb2 = false;
  private byte by = 11;
  private Byte by2 = 24;
  private char ch = 'a';
  private short sh = 563;
  private Short sh2 = 348;
  private long ll = 74565409L;
  private Long ll2 = 12334142L;
  private int variableName = 1;

  public String methodName(int x) {
    // This is a comment
    String val = "X";
    if (x <= 3 || x % 5 == 0) {
      val = "Three";
    }
    return "X";
  }
}
