import java.util.ArrayList;

class Teacher {
  String name;
  String designation = "Teacher";
  String collegeName = "Bedford";
  ArrayList<Object> skills = new ArrayList<Object>();

  public Teacher() {
  }

  public Teacher(String name) {
    this.name = name;
  }

  public Teacher(String name, String[] subjects) {

    this.name = name;

    for (String subject : subjects) {
      if (subject.equals("Physics")) {
        PhysicsTeacher pt = new PhysicsTeacher();
        skills.add(pt);
      }

      if (subject.equals("Maths")) {
        MathsTeacher mt = new MathsTeacher();
        skills.add(mt);
      }
    }
  }

  public String does() {
    return "Teaching";
  }

  public String getDesignation() {
    return designation;
  }

  public String getCollege() {
    return collegeName;
  }

}

class PhysicsTeacher extends Teacher {
  String mainSubject = "Physics";

  public String does() {
    return "Teaching " + mainSubject;
  }
}

class MathsTeacher extends Teacher {
  String mainSubject = "Maths";
  String specialism = "Pure Maths";

  public String getSpecialism() {
    return specialism;
  }
}
